# ps-lsof

 This project's goal is to develop a tool for being able to get a list of the packages
and files which own them that have been deleted or replaced after package upgrades.  
This will allow you do for example, on a server, know which services need to be restarted
for the updates to take effect.  This is exceedingly useful information.
This functionality was inspired by the zypper ps command which does something similar.  
### ps-lsof
  It relies of the `ps` command to get a list of the PID(proccess ID's) of all
currently running programs.  It
then uses `lsof` to get a list of all files open by that process by their PID.  
The end result of this is filtered out to remove unneeded information.

###### Output of **ps-lsof**

    PROCESS          FILENAME
    atom             /usr/share/mime/mime.cache
    cmst             /usr/share/icons/hicolor/icon-theme.cache
    gvfsd-http       /usr/lib/libsqlite3.so.0.8.6
    kactivitymanage  /usr/lib/libsqlite3.so.0.8.6
    kactivitymanage  /usr/share/mime/mime.cache
    korgac           /usr/share/mime/mime.cache
    kwin_x11         /usr/lib/libevdev.so.2.1.14

### Integrating with package managers
 The `lsof-ps` command in this project works, but wouldn't it be nice if it
integrated with our package manager and told us not just which files had been
changed on disk, but also which package they were from?
For Arch you can follow the instructions below for `pacman-ps`.

### pacman-ps
 To use this you can run: `pacman-ps`.  It will query pacman for a list of
packages and files they own using `pacman -Ql`.  It will then call
`ps-lsof`(must be in the same folder as pacman-ps.sh) and output something
like this:

    FILE                          PROCESS          PACKAGE
    /usr/lib/libsqlite3.so.0.8.6  gvfsd-http       sqlite
    /usr/lib/libsqlite3.so.0.8.6  kactivitymanage  sqlite
All that needs to be done is to download this project and run `makepkg` in the
directory.  Then you can run `sudo pacman -U pacman-ps*.pkg.tar.xz` to install it.

### Pacman Hook
 Every time a package is installed, a hook for pacman will update the database
of files and the packages which own them.  This is useful because if a package
is removed or some of the filenames in the previous version of the
package were renamed, the `pacman-ps` command will not be able to match up the
open files with the packages which own them.  The pacman hook will run
`pacman-posthook` which accepts a list of the installed packages via stdin
from pacman and then queries pacman about what files those packages contain.
It then adds them to the database.

### Files
 The database is located at `/var/cache/pacman-ps/files.db`.
### License
 Copyright (C) 2016  Samantha McVey <samantham@posteo.net>
This file and project are licensed under the GPLv2 or greater at your choice.
For more information view the license included or visit:
[https://www.gnu.org/licenses/gpl-2.0.html](https://www.gnu.org/licenses/gpl-2.0.html)
